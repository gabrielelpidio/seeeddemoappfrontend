

## Getting Started

To run the development server:

```bash
docker-compose up 
```

To run the production build:

In the docker file replace `CMD [CMD "npm" "run" "dev"]` with `CMD "npm" "run" "start"`

Then run 

```bash
docker-compose up
```

To install a new dependency:

```bash
docker-compose run nextjs npm install <your-dependency>
```
