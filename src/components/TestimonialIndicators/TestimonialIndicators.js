import React from "react";
import { useSpring, a } from "react-spring";

/**
 * Indicators based on an array and a index
 * @param {{data: Object, index:Number}}
 */
export const TestimonialIndicators = ({ data = [], index = 0 }) => {
  return (
    <div className="flex w-full gap-2">
      {data.map((_, i) => {
        return <IndicatorDot key={i} isActive={i === index} />;
      })}
    </div>
  );
};

/**
 *  Indicator dot with animation trough React Spring
 * @param {{isActive: boolean, }}
 */
const IndicatorDot = ({ isActive }) => {
  const spring = useSpring({
    backgroundColor: isActive ? "rgb(79, 70, 229)" : "rgb(209, 213, 219)",
  });
  return <a.span style={spring} className="rounded-full h-2 w-2" />;
};
