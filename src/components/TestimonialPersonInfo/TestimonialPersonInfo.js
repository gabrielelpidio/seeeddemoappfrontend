import React from "react";
import { a, useTransition } from "react-spring";

export const TestimonialPersonInfo = ({
  index = 0,
  testimonials = [{ name: "" }],
}) => {
  /**
   * Allows transition trough React Spring
   * more info: https://www.react-spring.io/docs/hooks/use-transition
   */
  const transition = useTransition(index, index, {
    from: {
      position: "absolute",
      opacity: 0,
    },
    enter: { opacity: 1, transform: "translate3d(0, 0, 0)" },
    leave: {
      opacity: 0,
    },
  });
  return (
    <div className="flex items-center gap-4 h-24 w-full max-w-7xl">
      {transition.map(({ item, key, props }) => (
        <a.div
          key={key}
          style={props}
          className="flex w-full items-center gap-4 "
        >
          <figure className=" h-12 w-12 md:h-24 md:w-24 border-gray-900 border-2 rounded-full overflow-hidden">
            <img
              className="w-full h-full"
              src={`https://ui-avatars.com/api/?name=${testimonials[item].name}&background=ffffff&bold=true`}
              alt=""
            />
          </figure>

          <span className="font-bold text-2xl">{testimonials[item].name}</span>
        </a.div>
      ))}
    </div>
  );
};
