import React, { useState, useEffect } from "react";

export const HomeForm = () => {
  const [name, setName] = useState(null);
  const [content, setContent] = useState(null);
  const [successful, setSuccessful] = useState(false);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  /**
   * Uses setTimeout to inform the user that the request was sended successfully
   * and let it do another request
   */
  useEffect(() => {
    let timeout;
    if (successful === true) {
      timeout = setTimeout(() => {
        setSuccessful(false);
        setName("");
        setContent("");
      }, 4000);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [successful]);

  /**
   * Handles controlled submit of the Form
   * @param {Event} e submit event
   */
  function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    fetch(process.env.NEXT_PUBLIC_API_URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ testimonial: { name: name, content: content } }),
    })
      .then(() => {
        setLoading(false);
        setSuccessful(true);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
        setError(true);
      });
  }

  return (
    <div className=" w-full md:w-1/2 flex items-center flex-wrap gap-8">
      <h2 className="w-full text-4xl text-center font-bold">
        Tell us your story
      </h2>
      <form
        onSubmit={handleSubmit}
        className=" flex-grow flex flex-col  bg-white rounded-xl gap-4 h-96 w-full  p-4 text-gray-900"
      >
        <input
          value={name}
          required={true}
          onChange={(e) => setName(e.target.value)}
          type="text"
          className="bg-indigo-100 h-12 rounded-md w-full p-2 placeholder-gray-400  font-semibold outline-none focus:border-indigo-800 border-transparent border-2"
          placeholder="What's your name?"
        />
        <textarea
          value={content}
          required={true}
          onChange={(e) => setContent(e.target.value)}
          className="bg-indigo-100 rounded-md w-full h-full p-2 placeholder-gray-400 resize-none outline-none focus:border-indigo-800 border-transparent border-2"
          placeholder="Share your experience"
        />
        <button
          className={`${
            successful
              ? "bg-green-500"
              : error
              ? "bg-yellow-500"
              : "bg-indigo-700"
          } text-white font-bold rounded-lg py-2 px-5 disabled:opacity-50`}
          disabled={content && name ? false : true}
        >
          {loading
            ? "Loading..."
            : successful
            ? "Sended"
            : error
            ? "Retry"
            : "Send"}
        </button>
      </form>
    </div>
  );
};
