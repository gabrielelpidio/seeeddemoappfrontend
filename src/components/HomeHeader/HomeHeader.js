import React from "react";

export const HomeHeader = () => {
  return (
    <section className=" flex self-start justify-center flex-wrap gap-5  w-full">
      <h1 className="text-6xl font-bold w-full text-center ">
        Stories of success
      </h1>
      <p className="text-2xl text-center">
        For us is very important to know the success that our customers has had
        with us in the past, here you will be able to leave your story of
        success with us and see other customers stories
      </p>
    </section>
  );
};
