import React from "react";

export const Layout = ({ children }) => {
  return (
    <div className="flex justify-center bg-indigo-800 h-screen text-white overflow-y-auto">
      {children}
    </div>
  );
};
