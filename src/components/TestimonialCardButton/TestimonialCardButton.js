import React from "react";

export const TestimonialCardButton = ({ children, onClick = () => {} }) => {
  return (
    <button
      onClick={onClick}
      className="flex justify-center items-center h-12 w-12 rounded-full border-2 border-gray-900 focus:outline-none relative z-10"
    >
      {children}
    </button>
  );
};
