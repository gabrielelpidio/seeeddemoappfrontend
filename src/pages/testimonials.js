import React, { useEffect, useState } from "react";
import {
  AiOutlineArrowLeft,
  AiOutlineArrowRight,
  AiOutlineHome,
} from "react-icons/ai";
import useMeasure from "react-use-measure";
import Link from "next/link";
import { a, useSpring, useTransition } from "react-spring";
import { TestimonialCardButton } from "../components/TestimonialCardButton/TestimonialCardButton";
import { TestimonialPersonInfo } from "../components/TestimonialPersonInfo/TestimonialPersonInfo";
import { TestimonialIndicators } from "../components/TestimonialIndicators/TestimonialIndicators";

const Testimonials = () => {
  /**
   * Actual index of the data
   */
  const [index, setIndex] = useState(0);

  /**
   * Utility to get updated measurements of an element
   */
  const [ref, bounds] = useMeasure();

  /**
   * Gets the data from the URL specified in the env variable NEXT_PUBLIC_API_URL
   */
  const { data: testimonials, isLoading, isError } = useGetData(
    process.env.NEXT_PUBLIC_API_URL
  );

  /**
   * Animates the height of the parent using the height of the children
   */
  const height = useSpring({ height: bounds.height });

  /**
   * Sets the next index and if it's the final sets 0
   */
  function handleNextClick() {
    setIndex((index) => {
      if (index === testimonials.length - 1) {
        return 0;
      }
      return index + 1;
    });
  }

  /**
   * Sets the previous index and if it's the initial sets the final index of the array
   */

  function handlePrevClick() {
    setIndex((index) => {
      if (index === 0) {
        return testimonials.length - 1;
      }
      return index - 1;
    });
  }

  if (isError) {
    return "An error has ocurred retrieving data";
  }

  if (isLoading) {
    return "Loading...";
  }

  return (
    <>
      <Link href="/">
        <a className="absolute top-2 left-4 " href="">
          <AiOutlineArrowLeft className="sm:text-3xl text-2xl" />
        </a>
      </Link>
      <div className="w-11/12 self-center flex items-center">
        <a.div
          className="self-center  w-full overflow-hidden bg-white"
          style={height}
        >
          <article
            ref={ref}
            className="flex flex-col gap-y-12 max-w-full justify-center items-center self-center text text-gray-800  p-4 md:p-16 w-full"
          >
            <section className="flex  flex-wrap w-full justify-between items-center gap-8 relative">
              <TestimonialPersonInfo
                index={index}
                testimonials={testimonials}
              />
              <div className="flex gap-4">
                <TestimonialCardButton>
                  <AiOutlineArrowLeft
                    onClick={handlePrevClick}
                    className="text-3xl"
                  />
                </TestimonialCardButton>
                <TestimonialCardButton onClick={handleNextClick}>
                  <AiOutlineArrowRight className="text-3xl" />
                </TestimonialCardButton>
              </div>
            </section>
            <p className="font-semibold w-full h-full max-h-48 md:max-h-96 overflow-y-auto rounded-sm">
              {testimonials[index].content}
            </p>
            <TestimonialIndicators data={testimonials} index={index} />
          </article>
        </a.div>
      </div>
    </>
  );
};

/**
 * Gets data from a given url and puts it inside a state
 * @param {String} url The url where the function will get the data
 * @returns {{data: Object, isLoading: boolean, error: Error|null}}
 * an Object with data, loading state and an error if it occurs
 */
const useGetData = (url = "") => {
  if (url === "") {
    setError(true);
    return {
      data: data,
      isLoading: !error && !data,
      isError: error,
    };
  }
  const [data, setData] = useState([{}]);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(url)
      .then((data) => data.json())
      .then((t) => {
        setData(t.body);
      })
      .catch((e) => {
        setError(e);
      });
  }, []);

  return {
    data: data,
    isLoading: !error && !data,
    isError: error,
  };
};

export default Testimonials;
