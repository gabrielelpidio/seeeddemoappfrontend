import Head from "next/head";
import { Layout } from "../components/Layout/Layout";
import { AiOutlineArrowRight } from "react-icons/ai";
import Link from "next/link";
import { HomeHeader } from "../components/HomeHeader/HomeHeader";
import { HomeForm } from "../components/HomeForm/HomeForm";

export default function Home() {
  return (
    <>
      <Head>
        <title>Leave your testimonial</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className=" flex md:text-xl flex-wrap py-16 overflow-y-auto justify-center items-center mx-6 max-w-6xl gap-y-24">
        <HomeHeader />
        <section className="flex flex-wrap  justify-evenly w-full md:w-5/6 self-start items-center gap-16">
          <Link href="/testimonials">
            <a className="text-4xl w-72 font-bold flex justify-center items-center underline">
              <span className="w-2/3">What our customers are saying</span>
              <AiOutlineArrowRight />
            </a>
          </Link>
          <HomeForm></HomeForm>
        </section>
      </main>
    </>
  );
}
